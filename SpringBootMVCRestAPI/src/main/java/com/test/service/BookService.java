package com.test.service;

import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import com.test.model.Book;
import com.test.model.Books;

@Repository
@Service
public class BookService {
	
	private static Books data = new Books();
	
	static
	{
		data.getBkList().add(new Book(1, "Java Notes", 345));
		data.getBkList().add(new Book(2, "Geetha", 234));
		data.getBkList().add(new Book(3, "Ramayan", 342));
		data.getBkList().add(new Book(4, "Spring notes", 844));
		data.getBkList().add(new Book(5, "Shivapuranam", 8382));
	}
	
	//read book data
	public Books getAllBooks()
	{
		return data;
	}
	
	//add new book
	public void addBook(Book bk)
	{
		data.getBkList().add(bk);
	}
	
	//update book data
	public String updateBook(Book bk)
	{
		for(int i = 0; i<data.getBkList().size(); i++)
		{
			Book b = data.getBkList().get(i);
			if(b.getId().equals(bk.getId()))
			{
				data.getBkList().set(i, bk);
			}
		}
		
		return "the given id is not available";
	}
	
	//delete book data
	public String deleteBookData(Integer id)
	{
		for(int i = 0; i<data.getBkList().size(); i++)
		{
			Book b = data.getBkList().get(i);
			
			if(b.getId().equals(id))
			{
				data.getBkList().remove(i);
			}
		}
		
		return "the given id is not available";
	}
}
