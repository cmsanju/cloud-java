package com.test.model;

import java.util.ArrayList;
import java.util.List;

public class Books {
	
	private List<Book> bkList;

	public List<Book> getBkList() {
		
		if(bkList == null)
		{
			bkList = new ArrayList<>();
		}
		return bkList;
	}

	public void setBkList(List<Book> bkList) {
		this.bkList = bkList;
	}
	
}