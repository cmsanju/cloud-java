package com.test.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.test.model.Book;
import com.test.model.Books;
import com.test.service.BookService;

@RestController
public class BookController {
	
	@Autowired
	private BookService bookService;
	
	@GetMapping(value = "/listbooks", produces = "application/json")
	public Books getAllBooks()
	{
		return bookService.getAllBooks();
	}
	
	@PostMapping(value = "/addbook", consumes = "application/json")
	public Books createBook(@RequestBody Book bk)
	{
		int id = bookService.getAllBooks().getBkList().size()+1;
		
		bk.setId(id);
		
		bookService.addBook(bk);
		
		return bookService.getAllBooks();
	}
	
	@PutMapping(value = "/updatebook/{id}", consumes = "application/json")
	public Books updateBook(@RequestBody Book bk, @PathVariable("id") Integer id)
	{
		bk.setId(id);
		
		bookService.updateBook(bk);
		
		return bookService.getAllBooks();
	}
	
	@DeleteMapping(value = "/deletebook/{id}", produces = "application/json")
	public Books deleteBook(@PathVariable("id") Integer id)
	{
		bookService.deleteBookData(id);
		
		return bookService.getAllBooks();
	}
}
